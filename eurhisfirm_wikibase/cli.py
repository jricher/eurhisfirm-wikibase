import datetime
import logging
import sys
from pathlib import Path
from typing import Dict, Iterator, List, Optional, Tuple

import numpy as np
import pandas as pd
import typer
from environs import Env
from ruamel.yaml import YAML
from wikidataintegrator.wdi_core import WDApiError, WDItemEngine
from wikidataintegrator.wdi_helpers import id_mapper
from wikidataintegrator.wdi_login import WDLogin

from . import model
from .wdi_utils import iter_info_item_id_from_wikibase
from .wikibase_utils import create_entity

BASE_ENTITIES_MODEL_FILE = Path("base_entities.yml")
BASE_ENTITIES_GEN_PY_FILE = Path("base_entities_gen.py")

DEFAULT_LOG_LEVEL = "INFO"

log = logging.getLogger(__name__)

app = typer.Typer()
env = Env()
env.read_env()


def load_entities(file_path: Path) -> model.Entities:
    """Load entities from yaml file."""
    yaml = YAML(typ="safe")
    return model.Entities.parse_obj(yaml.load(file_path.read_text()))


def generate_base_entities_python_file(
    file_path: Path, base_entities_model: model.Entities
):
    """Generate base entitities python file."""

    header_comment = f"""# This file is automatically generated
# Please don't edit it manually
# Last generation on {datetime.datetime.now().isoformat()}"""

    with file_path.open("wt", encoding="utf-8") as fd:
        fd.write(header_comment + "\n")
        if base_entities_model.items:
            fd.write("\nclass BaseItem:\n")
            for item_name, item in sorted(base_entities_model.items.items()):
                fd.write(f"    {item_name} = '{item.id}'\n")
        if base_entities_model.properties:
            fd.write("\nclass BaseProperty:\n")
            for property_name, prop in sorted(base_entities_model.properties.items()):
                fd.write(f"    {property_name} = '{prop.id}'\n")


def configure_logging_from_args(log_level: str):
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(log_level))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )


def merge_models(
    base_entities: model.Entities, other_entities_files: Iterator[Path]
) -> model.Entities:
    """Build entities model from base entities and specific ones."""

    merged_entities = base_entities

    for other_entities_file in other_entities_files:

        entities = load_entities(other_entities_file)

        # Merge items
        if entities.items:
            for name, item_model in entities.items.items():
                if name in merged_entities.items:
                    log.warning(
                        "%s: ignoring duplicated declaration of [%s] item",
                        other_entities_file,
                        name,
                    )
                    continue
                merged_entities.items[name] = item_model

        # Merge properties
        if entities.properties:
            for name, property_model in entities.properties.items():
                if name in merged_entities.properties:
                    log.warning(
                        "%s: ignoring duplicated declaration of [%s] property",
                        other_entities_file,
                        name,
                    )
                    continue
                merged_entities.properties[name] = property_model

    return merged_entities


def iter_base_entities_by_lang_label(
    item_engine: WDItemEngine,
    entity_label: str,
    entity_label_lang: str,
    entity_type: str,
    sparql_endpoint_url: str,
):
    query = f"""
    SELECT *
    WHERE {{
        ?item rdfs:label "{entity_label}"@{entity_label_lang}
    }}
    """

    def value_func(result: Dict):
        return None

    prefix = "Q" if entity_type == "item" else "P"

    for _, entity_id in iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    ):
        if entity_id.startswith(prefix):
            yield entity_id


def fetch_or_create_base_entities(
    base_entities_model: model.Entities,
    item_engine: WDItemEngine,
    login_instance: WDLogin,
    wikibase_api_url: str,
    sparql_endpoint_url: str,
    language_to_use: str,
    existing_entities_map: Dict[str, Dict[str, str]] = None,
):
    """Fetch existing entities, create missing ones."""

    existing_entities_map = (
        {"properties": {}, "items": {}}
        if existing_entities_map is None
        else existing_entities_map
    )

    # First check properties
    for name, property_model in base_entities_model.properties.items():

        # Re-use id if already defined
        if name in existing_entities_map["properties"]:
            property_model.id = existing_entities_map["properties"][name]
            log.info("Re-using base property %s: %s", name, property_model.id)
            continue

        # Check if property has a label in default language
        if (
            not property_model.labels
            or property_model.labels.get(language_to_use) is None
        ):
            log.error(
                "Property %r defines no [%s] label, skipping", name, language_to_use
            )
            continue

        log.info("Syncing base property %s", name)

        # Look for properties by english label
        prop_ids = list(
            iter_base_entities_by_lang_label(
                item_engine,
                property_model.labels[language_to_use],
                language_to_use,
                "property",
                sparql_endpoint_url,
            )
        )

        # If several properties found, just warn and skip
        if len(prop_ids) > 1:
            log.warn(
                "Skip property %r, %d properties already share same [%s] label: %s",
                label,
                len(prop_ids),
                language_to_use,
                ", ".join(prop_ids),
            )
            continue

        # Property found, don't go further
        if len(prop_ids) == 1:
            log.debug("Property already exists", name)
            property_model.id = prop_ids[0]
            continue

        # Not found, just create it
        try:
            property_model.id = create_entity(
                login_instance,
                item_engine,
                property_model.labels,
                property_model.descriptions,
                property_model.aliases,
                "property",
                datatype=property_model.type,
            )
            log.debug("Property successfully created.")
        except WDApiError as exc:
            log.error("Error creating '%s' base property: %r", name, exc)

    # Then check items
    for name, item_model in base_entities_model.items.items():

        # Re-use item id if previously defined
        if name in existing_entities_map["items"]:
            item_model.id = existing_entities_map["items"][name]
            log.info("Re-using base item %s: %s", name, item_model.id)
            continue

        # Check if item has a label in default language
        if not item_model.labels or item_model.labels.get(language_to_use) is None:
            log.error("Item %r defines no [%s] label, skipping", name, language_to_use)
            continue

        log.info("Syncing base item %s", name)

        # Look for existing items by default language item
        label = item_model.labels[language_to_use]
        item_ids = list(
            iter_base_entities_by_lang_label(
                item_engine, label, language_to_use, "item", sparql_endpoint_url
            )
        )

        # If several items found, just warn and skip
        if len(item_ids) > 1:
            log.warn(
                "Skip item %r, %d items already share same [%s] label: %s",
                label,
                len(item_ids),
                language_to_use,
                ", ".join(item_ids),
            )
            continue

        # Item found, don't go further
        if len(item_ids) == 1:
            log.debug("Item already exists", name)
            item_model.id = item_ids[0]
            continue

        # Not found, just create it
        try:
            item_model.id = create_entity(
                login_instance,
                item_engine,
                item_model.labels,
                item_model.descriptions,
                item_model.aliases,
                "item",
            )
            log.debug("Item successfully created.")
        except WDApiError as exc:
            log.error("Error creating '%s' base item: %r", name, exc)


@app.command()
def sync_base_entities(
    base_entities_model_file: Path = Path(env("BASE_ENTITIES_MODEL_FILE"))
    if env("BASE_ENTITIES_MODEL_FILE")
    else BASE_ENTITIES_MODEL_FILE,
    wikibase_api_url: str = env("WIKIBASE_API_URL"),
    mediawiki_bot_username: str = env("MEDIAWIKI_BOT_USERNAME"),
    mediawiki_bot_password: str = env("MEDIAWIKI_BOT_PASSWORD"),
    sparql_endpoint_url: str = env("SPARQL_ENDPOINT_URL"),
    default_language: str = env("DEFAULT_LANGUAGE"),
    force: bool = typer.Option(
        False, "--force", help=f"Force {BASE_ENTITIES_GEN_PY_FILE} re-generation.",
    ),
    log_level: str = typer.Option(
        DEFAULT_LOG_LEVEL, "--log", help="Level of logging messages."
    ),
):
    """Synchronize model base entities."""

    configure_logging_from_args(log_level)

    login_instance = WDLogin(
        mediawiki_api_url=wikibase_api_url,
        user=mediawiki_bot_username,
        pwd=mediawiki_bot_password,
    )

    LocalItemEngine = WDItemEngine.wikibase_item_engine_factory(
        mediawiki_api_url=wikibase_api_url, sparql_endpoint_url=sparql_endpoint_url,
    )

    # Load base entities model
    if not base_entities_model_file.exists():
        log.error(
            "Entities model file [%s] not found. Stopping here.",
            str(base_entities_model_file),
        )
        sys.exit(1)
    base_entities_model = load_entities(base_entities_model_file)

    # Merge with specific models
    base_entities_model = merge_models(
        base_entities_model, Path(".").glob("base_entities_*.yml")
    )

    # Re-use already defined properties and items
    def entity_class_to_dict(clazz):
        return {k: v for k, v in clazz.__dict__.items() if not k.startswith("--")}

    existing_entities_map = None
    if not force and BASE_ENTITIES_GEN_PY_FILE.exists():
        import base_entities_gen

        existing_entities_map = {
            "properties": entity_class_to_dict(base_entities_gen.BaseProperty),
            "items": entity_class_to_dict(base_entities_gen.BaseItem),
        }

    # Fetch existing base entities, create missing ones
    fetch_or_create_base_entities(
        base_entities_model,
        LocalItemEngine,
        login_instance,
        wikibase_api_url,
        sparql_endpoint_url,
        default_language,
        existing_entities_map,
    )

    # Generate python reference file defining base item and property ids
    generate_base_entities_python_file(BASE_ENTITIES_GEN_PY_FILE, base_entities_model)


@app.command()
def import_csv_data(
    provider: str = typer.Argument(...),
    csv_files_dir: Path = typer.Option(env("CSV_FILES_DIR"), file_okay=False),
    wikibase_api_url: str = env("WIKIBASE_API_URL"),
    mediawiki_bot_username: str = env("MEDIAWIKI_BOT_USERNAME"),
    mediawiki_bot_password: str = env("MEDIAWIKI_BOT_PASSWORD"),
    sparql_endpoint_url: str = env("SPARQL_ENDPOINT_URL"),
    default_language: str = env("DEFAULT_LANGUAGE"),
    available_languages_str: str = env("AVAILABLE_LANGUAGES"),
    only: List[int] = None,
    log_level: str = typer.Option(
        DEFAULT_LOG_LEVEL, "--log", help="level of logging messages"
    ),
):
    """Import/update entities from CSV files to wikibase."""
    configure_logging_from_args(log_level)

    if available_languages_str is None:
        available_languages_str = ""
    language_list = [
        lang.strip() for lang in available_languages_str.split(",") if lang
    ]

    if not BASE_ENTITIES_GEN_PY_FILE.exists():
        log.error(
            f"{BASE_ENTITIES_GEN_PY_FILE} file is missing, please run sync-base-entities first!"
        )
        sys.exit(1)

    login_instance = WDLogin(
        mediawiki_api_url=wikibase_api_url,
        user=mediawiki_bot_username,
        pwd=mediawiki_bot_password,
    )

    LocalItemEngine = WDItemEngine.wikibase_item_engine_factory(
        mediawiki_api_url=wikibase_api_url, sparql_endpoint_url=sparql_endpoint_url,
    )

    if provider in ("scob", "dfih"):

        if provider == "scob":
            from .scob_import import import_csv_data
        else:
            from .dfih_import import import_csv_data

        import_csv_data(
            default_language,
            language_list,
            csv_files_dir,
            LocalItemEngine,
            sparql_endpoint_url,
            login_instance,
        )

    else:
        log.error("Unknown provider [%s]", provider)


def cli():
    app()


if __name__ == "__main__":
    cli()
