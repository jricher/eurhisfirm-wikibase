"""YAML mapping model definition classes.

See also:
- https://www.mediawiki.org/wiki/Wikibase/DataModel/Primer
- https://www.mediawiki.org/wiki/Wikibase/DataModel
"""

from typing import Dict, List, Optional

from pydantic import BaseModel, Field

LanguageName = str
MultiLanguageDict = Dict[LanguageName, str]
MultiLangTermListDict = Dict[LanguageName, List[str]]


class Entity(BaseModel):
    labels: MultiLanguageDict
    id: Optional[str] = None
    descriptions: Optional[MultiLanguageDict] = None
    aliases: Optional[MultiLangTermListDict] = None


class Property(Entity):
    type: str


class Entities(BaseModel):
    items: Optional[Dict[str, Entity]]
    properties: Optional[Dict[str, Property]]
    aliases: Optional[MultiLangTermListDict] = None
