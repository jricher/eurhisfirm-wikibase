#!/usr/bin/env python3
import datetime
import logging
from typing import Any, Dict, List, Optional

import pandas as pd
from wikidataintegrator.wdi_core import (
    WDApiError,
    WDItemEngine,
    WDItemID,
    WDString,
    WDTime,
)
from wikidataintegrator.wdi_login import WDLogin

from base_entities_gen import BaseProperty

WIKIBASE_LABEL_MAX_LEN = 250
WIKIBASE_STRING_MAX_LEN = 400

log = logging.getLogger(__name__)


def format_wikibase_date(d: datetime.date) -> str:
    """Format dataframe date into wikibase date."""
    d = datetime.datetime.combine(d, datetime.datetime.min.time())
    d_str = d.isoformat() + "Z"
    if not (d_str.startswith("+") or d_str.startswith("-")):
        d_str = "+" + d_str
    return d_str


def database_references(
    database_entity_id: str, additional_references: List[Any] = None
):
    """Build source database reference."""
    if additional_references is None:
        additional_references = []
    reference = [
        WDItemID(database_entity_id, BaseProperty.SOURCE_DATABASE, is_reference=True),
        WDTime(
            format_wikibase_date(datetime.date.today()),
            BaseProperty.RETRIEVED,
            is_reference=True,
        ),
        *additional_references,
    ]
    return [reference]


def SafeWDString(value: str, prop_nr: str, **kwargs):
    """Create a new WikidataString, truncating value if len exceeds wikibase string length limit."""
    if type(value) != str:
        log.error("BAD VALUE TYPE [%s] [%r] [%s]", type(value), value, prop_nr)

    return WDString(truncate(value, WIKIBASE_STRING_MAX_LEN), prop_nr, **kwargs)


def truncate_label(str_value: str):
    """Truncate label to maximum length accepted by wikibase."""
    return truncate(str_value, WIKIBASE_LABEL_MAX_LEN)


def truncate(str_value: str, maxlen: int) -> str:
    if len(str_value) <= maxlen:
        return str_value
    return str_value[: maxlen - 6].rstrip() + " [...]"


def compute_item(
    item_engine: WDItemEngine,
    statements,
    item_label: str,
    item_id: str = None,
    item_label_lang: Any = None,  # can be either a language (str) or a language list (List[str])
    fast_run_base_filter: Dict[str, str] = None,
):
    """Factor code to compute a new (or to update) item."""

    if item_label_lang is None:
        raise ValueError("No lang defined in compute_item")

    options = {
        "data": statements,
    }
    if fast_run_base_filter:
        options["fast_run"] = True
        options["fast_run_base_filter"] = fast_run_base_filter

    item = (
        item_engine(item_id, **options)
        if item_id is not None
        else item_engine(new_item=True, **options)
    )

    # Set item label(s)
    lang_list = [item_label_lang] if type(item_label_lang) == str else item_label_lang
    for lang in lang_list:
        item.set_label(truncate_label(item_label), lang=lang)
    return item


def iter_info_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str, sparql_query: str, value_func
):
    """Help itering over (value, item_id) tuples.
    
       Please, use '?item' parameter in sparql query.
    """

    results = item_engine.execute_sparql_query(
        sparql_query, endpoint=sparql_endpoint_url
    )

    if "results" in results:
        for result in results["results"].get("bindings", []):
            item_id = result["item"]["value"].split("/")[-1]
            result_map = {
                name: result[name]["value"] for name in result if name != "item"
            }
            yield (value_func(result_map), item_id)


def write_item(
    item: WDItemEngine,
    login_instance: WDLogin,
    value: Any,
    item_type_label: str,
    check_if_necessary: bool = True,
    label: Optional[str] = None,
) -> Optional[str]:

    if check_if_necessary and not item.require_write:
        item_id = item.wd_item_id
        log.info(
            "Skip unchanged item %s corresponding to %s", item_id, value,
        )
        return None

    info = f"{value}: {label}" if label else value

    try:
        item_id = item.write(login_instance)
        log.info("New %s item %s: « %r »", item_type_label, item_id, info)
        return item_id

    except WDApiError as exc:
        log.error(
            "Error creating %s item corresponding to « %s »: %r",
            item_type_label,
            info,
            exc,
        )

    return None


def find_latest_corporation_name(names_df: pd.DataFrame):
    return names_df.iloc[-1].NAME


def date_qualifiers(
    row,
    none_if_empty: bool = False,
    is_qualifier: bool = True,
    references=None,
    start_date_property_id: str = BaseProperty.START_DATE,
    end_date_property_id: str = BaseProperty.END_DATE,
):
    """Build date qualifier list, return None if both start_date and end_date is empty."""

    qualifier_list = []
    if row.STARTDATE:
        qualifier_list.append(
            WDTime(
                format_wikibase_date(row.STARTDATE),
                start_date_property_id,
                is_qualifier=is_qualifier,
                references=references,
            )
        )
    if row.ENDDATE:
        qualifier_list.append(
            WDTime(
                format_wikibase_date(row.ENDDATE),
                end_date_property_id,
                is_qualifier=is_qualifier,
                references=references,
            )
        )

    if qualifier_list:
        return qualifier_list
    return None if none_if_empty else []
