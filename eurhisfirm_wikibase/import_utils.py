#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
from typing import List, Set

import pandas as pd

from .wikibase_utils import normalize_space


def load_dataframe(
    file_path: Path,
    index_col: str,
    date_columns: List[str] = None,
    sort_columns: List[str] = None,
    text_columns: Set[str] = None,
) -> pd.DataFrame:
    """Load pandas data frame from CSV."""

    def parse_french_date(v):
        if isinstance(v, float):
            return None
        return pd.datetime.strptime(v, "%d/%m/%Y").date()

    options = {}
    if date_columns:
        options["parse_dates"] = date_columns
        options["date_parser"] = parse_french_date

    df = pd.read_csv(file_path, index_col=index_col, **options)

    if sort_columns:
        df = df.sort_values(by=sort_columns)

    if text_columns:
        for text_column in text_columns:
            df[text_column] = df[text_column].apply(str).apply(normalize_space)

    return df.replace({"nan": None})


def empty_string(value: str) -> bool:
    return value is None or len(value) == 0
