#!/usr/bin/env python3
import logging
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import pandas as pd
from wikidataintegrator.wdi_core import (
    WDApiError,
    WDExternalID,
    WDItemEngine,
    WDItemID,
    WDTime,
)
from wikidataintegrator.wdi_helpers import id_mapper
from wikidataintegrator.wdi_login import WDLogin

from base_entities_gen import BaseItem, BaseProperty

from .import_utils import empty_string, load_dataframe
from .wdi_utils import (
    SafeWDString,
    compute_item,
    database_references,
    date_qualifiers,
    find_latest_corporation_name,
    iter_info_item_id_from_wikibase,
    write_item,
)

DFIH_LABEL_LANGUAGE = "fr"


log = logging.getLogger(__name__)


def dfih_references(additional_references: List[Any] = None):
    return database_references(
        BaseItem.DFIH_DATABASE, additional_references=additional_references
    )


def load_corporations(csv_file: Path):
    return load_dataframe(
        csv_file,
        "ID",
        date_columns=["STARTDATE", "ENDDATE"],
        text_columns=["PUBLIC_STATUS"],
    )


def load_corporation_names(csv_file: Path):
    return load_dataframe(
        csv_file,
        "CORPORATION",
        ["STARTDATE", "ENDDATE"],
        ["CORPORATION", "STARTDATE"],
        {"NAME", "SOURCE"},
    )


def load_corporation_locations(csv_file: Path):
    return load_dataframe(
        csv_file,
        "CORPORATION",
        [],
        ["CORPORATION", "IN_YEARBOOK_FROM"],
        {"LOCATION_TYPE", "ADDRESS", "CITY", "DISTRICT", "COUNTRY"},
    )


def load_corporation_juridical_status(csv_file: Path):
    return load_dataframe(
        csv_file,
        "CORPORATION",
        ["STARTDATE", "ENDDATE"],
        ["CORPORATION", "STARTDATE"],
        {"JURIDICAL_STATUS", "COMMENTS", "SOURCE"},
    )


def load_persons(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "ID",
        ["STARTDATE", "ENDDATE"],
        ["ID", "STARTDATE"],
        {"NAME", "SOURCE", "COMMENTS"},
    )

    # Don't keep ids history
    df = df[df["NEW_ID"].isnull()]

    # Drop unwanted "NEW_ID" column
    df = df.drop("NEW_ID", axis=1)

    return df


def load_person_names(csv_file: Path):
    df = load_dataframe(
        csv_file, "ID", [], ["ID"], {"NAME_DFIH", "PRENOM(s)", "TITRE", "NOM"}
    )

    # Don't keep company names
    df = df[df["FLAG_ENTREPRISE"].isnull()]

    # Don't keep items to be checked
    df = df[df["A_VERIFIER"].isnull()]

    # Drop unwanted columns
    df = df.drop("FLAG_ENTREPRISE", axis=1)
    df = df.drop("A_VERIFIER", axis=1)

    return df


def load_person_function(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "PERSON",
        ["STARTDATE", "ENDDATE"],
        ["PERSON", "STARTDATE"],
        {"JOB", "SOURCE", "COMMENTS"},
    )

    # Drop unused OLD_PERSON_ID column
    df = df.drop("OLD_PERSON_ID", axis=1)

    return df


def iter_location_tuple_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LOCATION}.
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_ADDRESS} ?stmt1 {{
                ?item wdt:{BaseProperty.LOCATION_ADDRESS} ?address.
                ?stmt1 prov:wasDerivedFrom ?refnode1.
                ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_CITY} ?stmt2 {{
                ?item wdt:{BaseProperty.LOCATION_CITY} ?city.
                ?stmt2 prov:wasDerivedFrom ?refnode2.
                ?refnode2 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_DISTRICT} ?stmt3 {{
                ?item wdt:{BaseProperty.LOCATION_DISTRICT} ?district.
                ?stmt3 prov:wasDerivedFrom ?refnode3.
                ?refnode3 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_COUNTRY} ?stmt4 {{
                ?item wdt:{BaseProperty.LOCATION_COUNTRY} ?country.
                ?stmt4 prov:wasDerivedFrom ?refnode4.
                ?refnode4 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
            }}
        }}
    }}
    """

    def value_func(result):
        return (
            result.get("address"),
            result.get("city"),
            result.get("district"),
            result.get("country"),
        )

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_location_item(item_engine, address, city, district, country):

    statements = [
        WDItemID(
            BaseItem.LOCATION, BaseProperty.INSTANCE_OF, references=dfih_references(),
        )
    ]
    if not empty_string(address):
        statements.append(
            SafeWDString(
                address, BaseProperty.LOCATION_ADDRESS, references=dfih_references(),
            )
        )
    if not empty_string(city):
        statements.append(
            SafeWDString(
                city, BaseProperty.LOCATION_CITY, references=dfih_references(),
            )
        )
    if not empty_string(district):
        statements.append(
            SafeWDString(
                district, BaseProperty.LOCATION_DISTRICT, references=dfih_references(),
            )
        )
    if not empty_string(country):
        statements.append(
            SafeWDString(
                country, BaseProperty.LOCATION_COUNTRY, references=dfih_references(),
            )
        )

    # Location label is a concatenation of address, city, district, country
    item_label = ", ".join(
        v for v in (address, city, district, country) if v is not None
    )

    return compute_item(
        item_engine, statements, item_label, item_label_lang=DFIH_LABEL_LANGUAGE
    )


def create_location_items(
    corporation_locations_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
) -> Dict[Tuple[str, str, str, str], str]:

    # Get existing location items as a dict (address, city, district, country) -> location item id
    item_id_by_location_tuple = {
        loc_tuple: location_item_id
        for (loc_tuple, location_item_id) in iter_location_tuple_item_id_from_wikibase(
            item_engine, sparql_endpoint_url
        )
    }

    # Iterate on corporation locations
    for row in corporation_locations_df.itertuples():
        location_tuple = (row.ADDRESS, row.CITY, row.DISTRICT, row.COUNTRY)
        if not location_tuple in item_id_by_location_tuple:
            log.debug(location_tuple)
            item = create_location_item(item_engine, *location_tuple)

            item_id = write_item(
                item,
                login_instance,
                location_tuple,
                "location",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_location_tuple[location_tuple] = item_id

    return item_id_by_location_tuple


def iter_location_type_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LOCATION_TYPE}.
        ?item wdt:{BaseProperty.LOCATION_TYPE_LABEL} ?label
    }}
"""

    def value_func(result):
        return result["label"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_location_type_item(item_engine, location_type_label, location_type_id):
    """Create location type item"""

    statements = [
        WDItemID(
            BaseItem.LOCATION_TYPE,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        SafeWDString(
            location_type_label,
            BaseProperty.LOCATION_TYPE_LABEL,
            references=dfih_references(),
        ),
        WDExternalID(
            str(location_type_id),
            BaseProperty.DFIH_LOCATION_TYPE_ID,
            references=dfih_references(),
        ),
    ]
    return compute_item(
        item_engine,
        statements,
        location_type_label,
        item_label_lang=DFIH_LABEL_LANGUAGE,
    )


def create_location_type_items(
    corporation_locations_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
) -> Dict[str, str]:

    # Get existing location type items as a dict (location type label) -> location item id
    item_id_by_location_type = {
        location_type_label: location_item_id
        for (
            location_type_label,
            location_item_id,
        ) in iter_location_type_item_id_from_wikibase(item_engine, sparql_endpoint_url)
    }

    # Iterate on corporation locations
    for row in corporation_locations_df.itertuples():
        location_type = row.LOCATION_TYPE
        if not location_type in item_id_by_location_type:
            log.debug(location_type)
            item = create_location_type_item(
                item_engine, location_type, row.LOCATION_TYPES_ID
            )

            item_id = write_item(
                item,
                login_instance,
                location_type,
                "location type",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_location_type[location_type] = item_id

    return item_id_by_location_type


def create_legal_form_item(item_engine, legal_form_label):
    """Create legal form item"""

    statements = [
        WDItemID(
            BaseItem.LEGAL_FORM, BaseProperty.INSTANCE_OF, references=dfih_references(),
        ),
        SafeWDString(
            legal_form_label,
            BaseProperty.LEGAL_FORM_LABEL,
            references=dfih_references(),
        ),
    ]
    return compute_item(
        item_engine, statements, legal_form_label, item_label_lang=DFIH_LABEL_LANGUAGE
    )


def iter_legal_form_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LEGAL_FORM}.
        ?item p:{BaseProperty.LEGAL_FORM_LABEL} ?stmt {{
            ?item wdt:{BaseProperty.LEGAL_FORM_LABEL} ?label.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
        }}
    }}
"""

    def value_func(result):
        return result["label"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_legal_form_items(
    corporation_juridical_status_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):

    # Get existing legal form items as a dict legal form label -> legal form item id
    item_id_by_legal_form_label = {
        legal_form_label: legal_form_item_id
        for (
            legal_form_label,
            legal_form_item_id,
        ) in iter_legal_form_item_id_from_wikibase(item_engine, sparql_endpoint_url)
    }

    # Iterate on corporation juridical status
    for row in corporation_juridical_status_df.itertuples():
        legal_form_label = row.JURIDICAL_STATUS
        if not legal_form_label in item_id_by_legal_form_label:
            log.debug(legal_form_label)
            item = create_legal_form_item(item_engine, legal_form_label)

            item_id = write_item(
                item,
                login_instance,
                legal_form_label,
                "legal form",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_legal_form_label[legal_form_label] = item_id

    return item_id_by_legal_form_label


def create_corporation_item(
    label_language_list: List[str],
    item_engine: WDItemEngine,
    row,
    names_df: pd.DataFrame,
    juridical_status_df: Optional[pd.DataFrame],
    locations_df: Optional[pd.DataFrame],
    item_id_by_location_tuple: Dict[Tuple[str, str, str, str], str],
    item_id_by_location_type: Dict[str, str],
    item_id_by_legal_form_label: Dict[str, str],
) -> Tuple[str, str]:
    """Create or update a Wikibase item corresponding to a DFIH corporation."""

    def year_qualifier(year: int, prop_nr: str):
        """Create WDTime instance from year value."""
        return WDTime(
            f"+0000000{year}-00-00T00:00:00Z", prop_nr, precision=9, is_qualifier=True
        )

    statements: List[Any] = [
        WDItemID(BaseItem.CORP, BaseProperty.INSTANCE_OF, references=dfih_references())
    ]

    corp_references = dfih_references()

    # statements from dfih_corporation_sd_ed
    statements.extend(
        [
            WDExternalID(
                str(row.Index), BaseProperty.DFIH_CORP_ID, references=corp_references,
            ),
            *date_qualifiers(row, is_qualifier=False, references=corp_references,),
        ]
    )
    if not empty_string(row.PUBLIC_STATUS):
        statements.append(
            SafeWDString(
                row.PUBLIC_STATUS,
                BaseProperty.PUBLIC_STATUS,
                references=corp_references,
            )
        )

    # Name statements from dfih_corporation_names
    for name_row in names_df.itertuples():

        source_statements = (
            [SafeWDString(name_row.SOURCE, BaseProperty.SOURCE, is_reference=True)]
            if not empty_string(name_row.SOURCE)
            else None
        )

        statements.append(
            SafeWDString(
                name_row.NAME,
                BaseProperty.CORP_NAME,
                qualifiers=date_qualifiers(name_row, none_if_empty=True),
                references=dfih_references(additional_references=source_statements),
            )
        )

    # Juridical status statements from dfih_corporation_juridical_status
    if juridical_status_df is not None:
        for juridical_status_row in juridical_status_df.itertuples():

            legal_form_label = juridical_status_row.JURIDICAL_STATUS
            legal_form_item = item_id_by_legal_form_label.get(legal_form_label)
            if legal_form_item is None:
                continue

            # Date qualifiers
            qualifiers = date_qualifiers(juridical_status_row)

            # Comments qualifier
            comments_value = juridical_status_row.COMMENTS
            if comments_value:
                qualifiers.append(
                    SafeWDString(
                        comments_value, BaseProperty.COMMENTS, is_qualifier=True
                    )
                )

            # Source reference
            source_references = (
                [
                    SafeWDString(
                        juridical_status_row.SOURCE,
                        BaseProperty.STATED_IN,
                        is_reference=True,
                    )
                ]
                if not empty_string(juridical_status_row.SOURCE)
                else None
            )

            # Legal form
            legal_form_statement = WDItemID(
                legal_form_item,
                BaseProperty.CORP_LEGAL_FORM,
                qualifiers=qualifiers,
                references=corp_references,
            )
            statements.append(legal_form_statement)

    # Location statements from dfih_corporation_locations.csv
    if locations_df is not None:
        for location_row in locations_df.itertuples():

            location_tuple = (
                location_row.ADDRESS,
                location_row.CITY,
                location_row.DISTRICT,
                location_row.COUNTRY,
            )
            if location_tuple not in item_id_by_location_tuple:
                log.error("Can't find location %r", location_tuple)
                continue
            location_item_id = item_id_by_location_tuple[location_tuple]

            qualifiers = []

            location_type = location_row.LOCATION_TYPE
            if location_type not in item_id_by_location_type:
                log.error("Can't find location type %s", location_type)
            location_type_item_id = item_id_by_location_type.get(location_type)
            if location_type_item_id:
                qualifiers.append(
                    WDItemID(
                        location_type_item_id,
                        BaseProperty.CORP_LOCATION_TYPE,
                        is_qualifier=True,
                    )
                )

            if location_row.IN_YEARBOOK_FROM:
                qualifiers.append(
                    year_qualifier(
                        location_row.IN_YEARBOOK_FROM, BaseProperty.IN_YEARBOOK_FROM
                    )
                )
            if location_row.IN_YEARBOOK_TO:
                qualifiers.append(
                    year_qualifier(
                        location_row.IN_YEARBOOK_TO, BaseProperty.IN_YEARBOOK_TO
                    )
                )

            location_statement = WDItemID(
                location_item_id,
                BaseProperty.CORP_LOCATION,
                qualifiers=qualifiers,
                references=dfih_references(),
            )
            statements.append(location_statement)

    corporation_label = find_latest_corporation_name(names_df)
    return (
        compute_item(
            item_engine,
            statements,
            corporation_label,
            item_label_lang=label_language_list,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.CORP},
        ),
        corporation_label,
    )


def create_corporation_items(
    label_language_list: List[str],
    corporation_sd_ed_df: pd.core.frame.DataFrame,
    corporation_names_df: pd.core.frame.DataFrame,
    corporation_juridical_status_df: pd.core.frame.DataFrame,
    corporation_locations_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
    item_id_by_location_tuple: Dict[Tuple[str, str, str, str], str],
    item_id_by_location_type: Dict[str, str],
    item_id_by_legal_form_label: Dict[str, str],
):

    # Compute corporation id to item id dict
    item_id_by_corp_id = id_mapper(
        BaseProperty.DFIH_CORP_ID, endpoint=sparql_endpoint_url
    )
    if item_id_by_corp_id:
        item_id_by_corp_id = {int(k): v for k, v in item_id_by_corp_id.items()}
    if item_id_by_corp_id is None:
        item_id_by_corp_id = {}

    for row_id, row in enumerate(corporation_sd_ed_df.itertuples()):
        dfih_corporation_id = row.Index

        # Skip already created corporation
        existing_item_id = item_id_by_corp_id.get(dfih_corporation_id)
        if existing_item_id:
            continue

        # Skip corporation without names
        if dfih_corporation_id not in corporation_names_df.index:
            log.debug(
                "No names information for corporation [%d], skipping",
                dfih_corporation_id,
            )
            continue

        names_df = corporation_names_df.loc[[dfih_corporation_id]]

        locations_df = (
            corporation_locations_df.loc[[dfih_corporation_id]]
            if dfih_corporation_id in corporation_locations_df.index
            else None
        )
        if locations_df is None:
            log.debug(
                "No location information for corporation [%d]", dfih_corporation_id
            )

        juridical_status_df = (
            corporation_juridical_status_df.loc[[dfih_corporation_id]]
            if dfih_corporation_id in corporation_juridical_status_df.index
            else None
        )
        if juridical_status_df is None:
            log.debug(
                "No juridical status information for corporation [%d]",
                dfih_corporation_id,
            )

        item, item_label = create_corporation_item(
            label_language_list,
            item_engine,
            row,
            names_df,
            juridical_status_df,
            locations_df,
            item_id_by_location_tuple,
            item_id_by_location_type,
            item_id_by_legal_form_label,
        )

        item_id = write_item(
            item, login_instance, dfih_corporation_id, "corporation", label=item_label,
        )
        if item_id:
            item_id_by_corp_id[dfih_corporation_id] = item_id

    return item_id_by_corp_id


def create_position_item(item_engine, position_label):
    """Create position item"""

    statements = [
        WDItemID(
            BaseItem.POSITION, BaseProperty.INSTANCE_OF, references=dfih_references(),
        ),
        SafeWDString(
            position_label, BaseProperty.POSITION_TITLE, references=dfih_references(),
        ),
    ]
    return compute_item(
        item_engine, statements, position_label, item_label_lang=DFIH_LABEL_LANGUAGE
    )


def iter_position_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.POSITION}.
        ?item p:{BaseProperty.POSITION_TITLE} ?stmt {{
            ?item wdt:{BaseProperty.POSITION_TITLE} ?position_title.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.DFIH_DATABASE}".
        }}
    }}
    """

    def value_func(result):
        return result["position_title"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_position_items(
    person_function_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):

    # Get existing position items as a dict position item label -> position item id
    item_id_by_position_label = {
        position_title: position_item_id
        for (position_title, position_item_id) in iter_position_item_id_from_wikibase(
            item_engine, sparql_endpoint_url
        )
    }

    # Iterate on person function
    for row in person_function_df.itertuples():
        position_label = row.JOB
        if not position_label in item_id_by_position_label:
            log.debug(position_label)
            item = create_position_item(item_engine, position_label)

            item_id = write_item(item, login_instance, position_label, "position")
            if item_id:
                item_id_by_position_label[position_label] = item_id

    return item_id_by_position_label


def iter_person_function_statement(
    dfih_person_id: int,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
):

    for function_row in person_function_df.itertuples():
        dfih_corporation_id = function_row.CORPORATION
        corporation_item_id = item_id_by_corporation_id.get(dfih_corporation_id)
        if not corporation_item_id:
            log.error(
                "Can't find corporation [%d] for person [%d]",
                dfih_corporation_id,
                dfih_person_id,
            )
            continue

        qualifiers = []
        position_label = function_row.JOB
        if not position_label:
            log.error("No position label for person [%d]", dfih_person_id)
            continue

        position_item_id = item_id_by_position_label.get(position_label)
        if not position_item_id:
            log.error("Can't find position label '%s' item", position_label)
            continue

        qualifiers.append(
            WDItemID(position_item_id, BaseProperty.PERSON_FUNCTION, is_qualifier=True,)
        )
        qualifiers.extend(date_qualifiers(function_row))
        if function_row.COMMENTS:
            qualifiers.append(
                SafeWDString(
                    function_row.COMMENTS, BaseProperty.COMMENTS, is_qualifier=True,
                )
            )

        source_references = (
            [
                SafeWDString(
                    function_row.SOURCE, BaseProperty.STATED_IN, is_reference=True,
                )
            ]
            if not empty_string(function_row.SOURCE)
            else None
        )

        yield WDItemID(
            corporation_item_id,
            BaseProperty.PERSON_FUNCTION_CORPORATION,
            qualifiers=qualifiers,
            references=dfih_references(additional_references=source_references),
        )


def create_person_item(
    label_language_list: List[str],
    item_engine: WDItemEngine,
    row,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
    item_id: Optional[str],
) -> Tuple[str, str]:

    # statements from persons.csv
    person_references = dfih_references(
        additional_references=(
            [SafeWDString(row.SOURCE, BaseProperty.STATED_IN, is_reference=True)]
            if not empty_string(row.SOURCE)
            else None
        )
    )

    dfih_person_id = row.Index
    statements = [
        WDItemID(
            BaseItem.PERSON, BaseProperty.INSTANCE_OF, references=person_references,
        ),
        WDExternalID(
            str(dfih_person_id),
            BaseProperty.DFIH_PERSON_ID,
            qualifiers=[
                SafeWDString(row.NAME, BaseProperty.NAMED_AS, is_qualifier=True)
            ],
            references=person_references,
        ),
    ]
    statements.extend(
        date_qualifiers(
            row,
            is_qualifier=False,
            references=person_references,
            start_date_property_id=BaseProperty.BIRTH_DATE,
            end_date_property_id=BaseProperty.DEATH_DATE,
        )
    )
    if not empty_string(row.COMMENTS):
        statements.append(
            SafeWDString(
                row.COMMENTS, BaseProperty.COMMENTS, references=person_references,
            )
        )

    # Function statements from person_function
    if person_function_df is not None:

        for function_statement in iter_person_function_statement(
            dfih_person_id,
            person_function_df,
            item_id_by_corporation_id,
            item_id_by_position_label,
        ):
            statements.append(function_statement)

    person_label = row.NAME
    return (
        compute_item(
            item_engine,
            statements,
            person_label,
            item_label_lang=label_language_list,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.PERSON},
        ),
        person_label,
    )


def iter_person_dfih_id_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.PERSON}.
        ?item wdt:{BaseProperty.DFIH_PERSON_ID} ?dfih_person_id
    }}
    """

    def value_func(result):
        return result["dfih_person_id"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_person_items(
    label_language_list: List[str],
    persons_df: pd.core.frame.DataFrame,
    person_function_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
    item_id_by_corp_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
):

    # Get existing person items as a dict dfih person id -> item id
    item_id_by_person_id = {
        int(dfih_person_id): person_item_id
        for (
            dfih_person_id,
            person_item_id,
        ) in iter_person_dfih_id_item_id_from_wikibase(item_engine, sparql_endpoint_url)
    }

    # Iterate on person
    for row in persons_df.itertuples():

        # Skip already created person
        dfih_person_id = row.Index
        existing_item_id = item_id_by_person_id.get(dfih_person_id)
        if existing_item_id:
            continue

        if row.NAME is None:
            log.info("Skip Person without name [%d]", dfih_person_id)
            continue

        fonction_df = (
            person_function_df.loc[[dfih_person_id]]
            if dfih_person_id in person_function_df.index
            else None
        )

        item, item_label = create_person_item(
            label_language_list,
            item_engine,
            row,
            fonction_df,
            item_id_by_corp_id,
            item_id_by_position_label,
            item_id=existing_item_id,
        )

        write_item(
            item, login_instance, dfih_person_id, "person", label=item_label,
        )


def import_csv_data(
    default_language: str,
    language_list: List[str],
    csv_files_dir: Path,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):
    """Import dfih entities from CSV data."""

    # Here default language parameter value is ignored
    # in favour of DFIH_LABEL_LANGUAGE value

    # Load CSV sources
    corporation_sd_ed_df = load_corporations(
        csv_files_dir / "dfih_corporation_sd_ed.csv"
    )
    corporation_names_df = load_corporation_names(
        csv_files_dir / "dfih_corporation_names.csv"
    )
    corporation_locations_df = load_corporation_locations(
        csv_files_dir / "dfih_corporation_locations.csv"
    )
    corporation_juridical_status_df = load_corporation_juridical_status(
        csv_files_dir / "dfih_corporation_juridical_status.csv"
    )
    persons_df = load_persons(csv_files_dir / "dfih_persons.csv")
    person_function_df = load_person_function(
        csv_files_dir / "dfih_person_function.csv"
    )

    # Create items in wikibase

    # Create location items
    item_id_by_location_tuple = create_location_items(
        corporation_locations_df, item_engine, sparql_endpoint_url, login_instance,
    )

    # Create location type items
    item_id_by_location_type = create_location_type_items(
        corporation_locations_df, item_engine, sparql_endpoint_url, login_instance,
    )

    # Create legal form
    item_id_by_legal_form_label = create_legal_form_items(
        corporation_juridical_status_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
    )

    # Create corporation items
    item_id_by_corp_id = create_corporation_items(
        language_list,
        corporation_sd_ed_df,
        corporation_names_df,
        corporation_juridical_status_df,
        corporation_locations_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
        item_id_by_location_tuple,
        item_id_by_location_type,
        item_id_by_legal_form_label,
    )

    # Create person position items
    item_id_by_position_label = create_position_items(
        person_function_df, item_engine, sparql_endpoint_url, login_instance,
    )

    # Create person items
    create_person_items(
        language_list,
        persons_df,
        person_function_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
        item_id_by_corp_id,
        item_id_by_position_label,
    )
