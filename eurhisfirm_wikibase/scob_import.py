#!/usr/bin/environ python3
import datetime
import logging
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import pandas as pd
from wikidataintegrator.wdi_core import WDExternalID, WDItemEngine, WDItemID, WDTime
from wikidataintegrator.wdi_helpers import id_mapper
from wikidataintegrator.wdi_login import WDLogin

from base_entities_gen import BaseItem, BaseProperty

from .import_utils import empty_string, load_dataframe
from .wdi_utils import (
    SafeWDString,
    compute_item,
    database_references,
    date_qualifiers,
    find_latest_corporation_name,
    format_wikibase_date,
    iter_info_item_id_from_wikibase,
    write_item,
)

log = logging.getLogger(__name__)


def load_corporations(file_path: Path) -> pd.DataFrame:
    return load_dataframe(file_path, "ID", date_columns=["STARTDATE", "ENDDATE"])


def load_corporation_names(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"NAME"},
    )


def load_corporation_locations(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"ADDRESS", "CITY", "COUNTRY"},
    )


def load_corporation_legal_status(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"JURIDISCH_STATUUT", "COMMENTS", "SOURCE"},
    )


def load_persons(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "ID",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["ID"],
        text_columns={"NAME", "SOURCE", "COMMENTS", "GESLACHT"},
    )


def load_person_function(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "PERSON",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["PERSON", "STARTDATE"],
        text_columns={"JOB", "SOURCE", "COMMENTS"},
    )


def scob_references(additional_references=None):
    return database_references(
        BaseItem.SCOB_DATABASE, additional_references=additional_references
    )


def create_corporation_item(
    label_language_list: List[str],
    item_engine: WDItemEngine,
    row,
    names_df: pd.DataFrame,
    juridisch_statuut_df: Optional[pd.DataFrame],
    locations_df: Optional[pd.DataFrame],
    item_id_by_location_tuple: Dict[Tuple[str, str, str], str],
    item_id_by_legal_form_label: Dict[str, str],
    item_id: Optional[str],
) -> Tuple[str, str]:
    """Create or update a Wikibase item corresponding to a SCOB corporation.

    Also create the dependent items like locations.
    """
    statements: List[Any] = [
        WDItemID(BaseItem.CORP, BaseProperty.INSTANCE_OF, references=scob_references())
    ]

    # statements from scob_corporation_sd_ed
    statements.extend(
        [
            WDExternalID(
                str(row.Index), BaseProperty.SCOB_CORP_ID, references=scob_references(),
            ),
            *date_qualifiers(row, is_qualifier=False, references=scob_references()),
        ]
    )

    # Name statements from scob_corporation_names
    for name_row in names_df.itertuples():
        statements.append(
            SafeWDString(
                name_row.NAME,
                BaseProperty.CORP_NAME,
                qualifiers=date_qualifiers(name_row, none_if_empty=True,),
                references=scob_references(),
            )
        )

    # Legal form statements from scob_corporation_juridisch_statuut
    if juridisch_statuut_df is not None:
        for juridisch_statuut_row in juridisch_statuut_df.itertuples():

            legal_form_label = juridisch_statuut_row.JURIDISCH_STATUUT
            legal_form_item = item_id_by_legal_form_label.get(legal_form_label)
            if legal_form_item is None:
                continue

            # Date qualifiers
            qualifiers = date_qualifiers(juridisch_statuut_row,)

            # Comments qualifier
            comments_value = juridisch_statuut_row.COMMENTS
            if not empty_string(comments_value):
                qualifiers.append(
                    SafeWDString(
                        comments_value, BaseProperty.COMMENTS, is_qualifier=True
                    )
                )

            # Source reference
            source_value = juridisch_statuut_row.SOURCE
            source_references = (
                [SafeWDString(source_value, BaseProperty.STATED_IN, is_reference=True)]
                if not empty_string(source_value)
                else None
            )

            # Legal status
            legal_status_statement = WDItemID(
                legal_form_item,
                BaseProperty.CORP_LEGAL_FORM,
                qualifiers=qualifiers,
                references=scob_references(additional_references=source_references),
            )
            statements.append(legal_status_statement)

    # Location statements from scob_corporation_locations.csv
    if locations_df is not None:
        for location_row in locations_df.itertuples():

            location_tuple = (
                location_row.ADDRESS,
                location_row.CITY,
                location_row.COUNTRY,
            )
            if location_tuple not in item_id_by_location_tuple:
                log.error("Can't find location %r", location_tuple)
                continue
            location_item_id = item_id_by_location_tuple[location_tuple]

            location_statement = WDItemID(
                location_item_id,
                BaseProperty.CORP_LOCATION,
                qualifiers=date_qualifiers(location_row, none_if_empty=True,),
                references=scob_references(),
            )
            statements.append(location_statement)

    corporation_label = find_latest_corporation_name(names_df)
    return (
        compute_item(
            item_engine,
            statements,
            corporation_label,
            item_id,
            item_label_lang=label_language_list,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.CORP},
        ),
        corporation_label,
    )


def create_location_item(
    label_language: str,
    item_engine: WDItemEngine,
    address: str,
    city: str,
    country: str,
):

    statements = [
        WDItemID(
            BaseItem.LOCATION, BaseProperty.INSTANCE_OF, references=scob_references(),
        )
    ]
    if address:
        statements.append(
            SafeWDString(
                address, BaseProperty.LOCATION_ADDRESS, references=scob_references(),
            )
        )
    if city:
        statements.append(
            SafeWDString(
                city, BaseProperty.LOCATION_CITY, references=scob_references(),
            )
        )
    if country:
        statements.append(
            SafeWDString(
                country, BaseProperty.LOCATION_COUNTRY, references=scob_references(),
            )
        )

    # Location label is a concatenation of address, city, country
    item_label = ", ".join(v for v in (address, city, country) if v is not None)

    return compute_item(
        item_engine, statements, item_label, item_label_lang=label_language
    )


def create_legal_form_item(
    label_language: str, item_engine: WDItemEngine, legal_form_label: str
):
    """Create legal form item"""

    statements = [
        WDItemID(
            BaseItem.LEGAL_FORM, BaseProperty.INSTANCE_OF, references=scob_references(),
        ),
        SafeWDString(
            legal_form_label,
            BaseProperty.LEGAL_FORM_LABEL,
            references=scob_references(),
        ),
    ]
    return compute_item(
        item_engine, statements, legal_form_label, item_label_lang=label_language
    )


def create_position_item(
    label_language: str, item_engine: WDItemEngine, position_label: str
):
    """Create position item."""

    statements = [
        WDItemID(
            BaseItem.POSITION, BaseProperty.INSTANCE_OF, references=scob_references(),
        ),
        SafeWDString(
            position_label, BaseProperty.POSITION_TITLE, references=scob_references(),
        ),
    ]
    return compute_item(
        item_engine, statements, position_label, item_label_lang=label_language
    )


def iter_person_function_statement(
    scob_person_id: int,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
):

    for function_row in person_function_df.itertuples():
        scob_corporation_id = function_row.CORPORATION
        corporation_item_id = item_id_by_corporation_id.get(scob_corporation_id)
        if not corporation_item_id:
            log.error(
                "Can't find corporation [%d] for person [%d]",
                scob_corporation_id,
                scob_person_id,
            )
            continue

        qualifiers = []
        position_label = function_row.JOB
        if empty_string(position_label):
            log.error("No position label for person [%d]", scob_person_id)
            continue

        position_item_id = item_id_by_position_label.get(position_label)
        if not position_item_id:
            log.error("Can't find position label '%s' item", position_label)
            continue

        qualifiers.append(
            WDItemID(position_item_id, BaseProperty.PERSON_FUNCTION, is_qualifier=True,)
        )
        qualifiers.extend(date_qualifiers(function_row))
        if not empty_string(function_row.COMMENTS):
            qualifiers.append(
                SafeWDString(
                    function_row.COMMENTS, BaseProperty.COMMENTS, is_qualifier=True,
                )
            )

        source_references = (
            [
                SafeWDString(
                    function_row.SOURCE, BaseProperty.STATED_IN, is_reference=True,
                )
            ]
            if not empty_string(function_row.SOURCE)
            else None
        )

        yield WDItemID(
            corporation_item_id,
            BaseProperty.PERSON_FUNCTION_CORPORATION,
            qualifiers=qualifiers,
            references=scob_references(additional_references=source_references),
        )


def create_person_item(
    label_language_list: List[str],
    item_engine,
    row,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
    item_id: Optional[str],
) -> Tuple[str, str]:

    # statements from persons.csv
    scob_person_id = row.Index

    # Source info from SOURCE column
    person_references = scob_references(
        additional_references=[
            SafeWDString(row.SOURCE, BaseProperty.STATED_IN, is_reference=True)
        ]
        if not empty_string(row.SOURCE)
        else []
    )

    statements = [
        WDItemID(
            BaseItem.PERSON, BaseProperty.INSTANCE_OF, references=person_references,
        ),
        WDExternalID(
            str(scob_person_id),
            BaseProperty.SCOB_PERSON_ID,
            qualifiers=[
                SafeWDString(row.NAME, BaseProperty.NAMED_AS, is_qualifier=True),
            ],
            references=person_references,
        ),
        WDItemID(
            BaseItem.FEMALE if row.GESLACHT == "V" else BaseItem.MALE,
            BaseProperty.PERSON_GENDER,
            references=person_references,
        ),
    ]

    statements.extend(
        date_qualifiers(
            row,
            is_qualifier=False,
            references=person_references,
            start_date_property_id=BaseProperty.BIRTH_DATE,
            end_date_property_id=BaseProperty.DEATH_DATE,
        )
    )
    if not empty_string(row.COMMENTS):
        statements.append(
            SafeWDString(
                row.COMMENTS, BaseProperty.COMMENTS, references=person_references,
            )
        )

    # Function statements from person_function
    if person_function_df is not None:

        for function_statement in iter_person_function_statement(
            scob_person_id,
            person_function_df,
            item_id_by_corporation_id,
            item_id_by_position_label,
        ):
            statements.append(function_statement)

    person_label = row.NAME
    return (
        compute_item(
            item_engine,
            statements,
            person_label,
            item_id,
            item_label_lang=label_language_list,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.PERSON},
        ),
        person_label,
    )


def iter_location_tuple_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LOCATION}.
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_ADDRESS} ?stmt1 {{
                ?item wdt:{BaseProperty.LOCATION_ADDRESS} ?address.
                ?stmt1 prov:wasDerivedFrom ?refnode1.
                ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.SCOB_DATABASE}".
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_CITY} ?stmt2 {{
                ?item wdt:{BaseProperty.LOCATION_CITY} ?city.
                ?stmt2 prov:wasDerivedFrom ?refnode2.
                ?refnode2 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.SCOB_DATABASE}".
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_COUNTRY} ?stmt3 {{
                ?item wdt:{BaseProperty.LOCATION_COUNTRY} ?country.
                ?stmt3 prov:wasDerivedFrom ?refnode3.
                ?refnode3 pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.SCOB_DATABASE}".
            }}
        }}
    }}
    """

    def value_func(result):
        return (result.get("address"), result.get("city"), result.get("country"))

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_location_items(
    label_language: str,
    corporation_locations_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):

    # Get existing location items as a dict (address, city, country) -> location item id
    item_id_by_location_tuple = {
        loc_tuple: location_item_id
        for (loc_tuple, location_item_id) in iter_location_tuple_item_id_from_wikibase(
            item_engine, sparql_endpoint_url
        )
    }

    # Iterate on corporation locations
    for row in corporation_locations_df.itertuples():
        location_tuple = (row.ADDRESS, row.CITY, row.COUNTRY)
        if not location_tuple in item_id_by_location_tuple:
            log.debug(location_tuple)
            item = create_location_item(label_language, item_engine, *location_tuple)

            item_id = write_item(
                item,
                login_instance,
                location_tuple,
                "location",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_location_tuple[location_tuple] = item_id

    return item_id_by_location_tuple


def iter_legal_form_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LEGAL_FORM}.
        ?item p:{BaseProperty.LEGAL_FORM_LABEL} ?stmt {{
            ?item wdt:{BaseProperty.LEGAL_FORM_LABEL} ?label.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.SCOB_DATABASE}".
        }}
    }}
    """

    def value_func(result):
        return result["label"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_legal_form_items(
    label_language: str,
    corporation_juridisch_statuut_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):

    # Get existing legal form items as a dict legal form label -> legal form item id
    item_id_by_legal_form_label = {
        legal_form_label: legal_form_item_id
        for (
            legal_form_label,
            legal_form_item_id,
        ) in iter_legal_form_item_id_from_wikibase(item_engine, sparql_endpoint_url)
    }

    # Iterate on corporation juridisch statuut
    for row in corporation_juridisch_statuut_df.itertuples():
        legal_form_label = row.JURIDISCH_STATUUT
        if not legal_form_label in item_id_by_legal_form_label:
            log.debug(legal_form_label)
            item = create_legal_form_item(label_language, item_engine, legal_form_label)

            item_id = write_item(
                item,
                login_instance,
                legal_form_label,
                "legal form",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_legal_form_label[legal_form_label] = item_id

    return item_id_by_legal_form_label


def create_corporation_items(
    label_language_list: List[str],
    corporation_sd_ed_df: pd.core.frame.DataFrame,
    corporation_names_df: pd.core.frame.DataFrame,
    corporation_juridisch_statuut_df: pd.core.frame.DataFrame,
    corporation_locations_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
    item_id_by_location_tuple: Dict[Tuple[str, str, str], str],
    item_id_by_legal_form_label: Dict[str, str],
):

    # Compute corporation id to item id dict
    item_id_by_corp_id = id_mapper(
        BaseProperty.SCOB_CORP_ID, endpoint=sparql_endpoint_url
    )
    if item_id_by_corp_id:
        item_id_by_corp_id = {int(k): v for k, v in item_id_by_corp_id.items()}
    if item_id_by_corp_id is None:
        item_id_by_corp_id = {}

    for row_id, row in enumerate(corporation_sd_ed_df.itertuples()):
        scob_corporation_id = row.Index

        # Skip already created corporation
        existing_item_id = item_id_by_corp_id.get(scob_corporation_id)
        if existing_item_id:
            continue

        # Skip corporation without names
        if scob_corporation_id not in corporation_names_df.index:
            log.debug(
                "No names information for corporation [%d], skipping",
                scob_corporation_id,
            )
            continue

        names_df = corporation_names_df.loc[[scob_corporation_id]]

        locations_df = (
            corporation_locations_df.loc[[scob_corporation_id]]
            if scob_corporation_id in corporation_locations_df.index
            else None
        )
        if locations_df is None:
            log.debug(
                "No location information for corporation [%d]", scob_corporation_id
            )

        juridisch_statuut_df = (
            corporation_juridisch_statuut_df.loc[[scob_corporation_id]]
            if scob_corporation_id in corporation_juridisch_statuut_df.index
            else None
        )
        if juridisch_statuut_df is None:
            log.debug(
                "No legal form information for corporation [%d]", scob_corporation_id
            )

        item, item_label = create_corporation_item(
            label_language_list,
            item_engine,
            row,
            names_df,
            juridisch_statuut_df,
            locations_df,
            item_id_by_location_tuple,
            item_id_by_legal_form_label,
            item_id=existing_item_id,
        )

        item_id = write_item(
            item, login_instance, scob_corporation_id, "corporation", label=item_label
        )
        if item_id:
            # fullfill correspondance map
            item_id_by_corp_id[scob_corporation_id] = item_id

    return item_id_by_corp_id


def iter_person_scob_id_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.PERSON}.
        ?item wdt:{BaseProperty.SCOB_PERSON_ID} ?scob_person_id
    }}
    """

    def value_func(result):
        return result["scob_person_id"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def iter_position_item_id_from_wikibase(
    item_engine: WDItemEngine, sparql_endpoint_url: str
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.POSITION}.
        ?item p:{BaseProperty.POSITION_TITLE} ?stmt {{
            ?item wdt:{BaseProperty.POSITION_TITLE} ?position_title.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} "{BaseItem.SCOB_DATABASE}".
        }}
    }}
    """

    def value_func(result):
        return result["position_title"]

    return iter_info_item_id_from_wikibase(
        item_engine, sparql_endpoint_url, query, value_func
    )


def create_position_items(
    label_language: str,
    person_function_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):

    # Get existing position items as a dict position item label -> position item id
    item_id_by_position_label = {
        position_title: position_item_id
        for (position_title, position_item_id) in iter_position_item_id_from_wikibase(
            item_engine, sparql_endpoint_url
        )
    }

    # Iterate on person function
    for row in person_function_df.itertuples():
        position_label = row.JOB
        if not position_label in item_id_by_position_label:
            log.debug(position_label)
            item = create_position_item(label_language, item_engine, position_label)

            item_id = write_item(item, login_instance, position_label, "position")
            if item_id:
                item_id_by_position_label[position_label] = item_id

    return item_id_by_position_label


def create_person_items(
    label_language_list: List[str],
    persons_df: pd.core.frame.DataFrame,
    person_function_df: pd.core.frame.DataFrame,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
    item_id_by_corp_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
):

    # Get existing person items as a dict scob person id -> item id
    item_id_by_person_id = {
        int(scob_person_id): person_item_id
        for (
            scob_person_id,
            person_item_id,
        ) in iter_person_scob_id_item_id_from_wikibase(item_engine, sparql_endpoint_url)
    }

    # Iterate on person
    for row in persons_df.itertuples():

        # Skip already created person
        scob_person_id = row.Index
        existing_item_id = item_id_by_person_id.get(scob_person_id)
        if existing_item_id:
            continue

        if row.NAME is None:
            log.info("Skip Person without name [%d]", scob_person_id)
            continue

        fonction_df = (
            person_function_df.loc[[scob_person_id]]
            if scob_person_id in person_function_df.index
            else None
        )

        item, item_label = create_person_item(
            label_language_list,
            item_engine,
            row,
            fonction_df,
            item_id_by_corp_id,
            item_id_by_position_label,
            item_id=existing_item_id,
        )
        write_item(item, login_instance, scob_person_id, "person", label=item_label)


def import_csv_data(
    default_language: str,
    language_list: List[str],
    csv_files_dir: Path,
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    login_instance: WDLogin,
):
    """Import scob entities from CSV data."""

    # Load CSV sources
    corporation_sd_ed_df = load_corporations(
        csv_files_dir / "scob_corporation_sd_ed.csv"
    )
    corporation_names_df = load_corporation_names(
        csv_files_dir / "scob_corporation_names.csv"
    )
    corporation_locations_df = load_corporation_locations(
        csv_files_dir / "scob_corporation_locations.csv"
    )
    corporation_juridisch_statuut_df = load_corporation_legal_status(
        csv_files_dir / "scob_corporation_juridisch_statuut.csv"
    )
    persons_df = load_persons(csv_files_dir / "scob_persons.csv")
    person_function_df = load_person_function(
        csv_files_dir / "scob_person_function.csv"
    )

    # Create location items
    item_id_by_location_tuple = create_location_items(
        default_language,
        corporation_locations_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
    )

    # Create legal form items
    item_id_by_legal_form_label = create_legal_form_items(
        default_language,
        corporation_juridisch_statuut_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
    )

    # Create corporation items
    item_id_by_corp_id = create_corporation_items(
        language_list,
        corporation_sd_ed_df,
        corporation_names_df,
        corporation_juridisch_statuut_df,
        corporation_locations_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
        item_id_by_location_tuple,
        item_id_by_legal_form_label,
    )

    # Create person position items
    item_id_by_position_label = create_position_items(
        default_language,
        person_function_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
    )

    # Create person items
    create_person_items(
        language_list,
        persons_df,
        person_function_df,
        item_engine,
        sparql_endpoint_url,
        login_instance,
        item_id_by_corp_id,
        item_id_by_position_label,
    )
