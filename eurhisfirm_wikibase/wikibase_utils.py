import re
from typing import Dict, List

from .wdi_utils import truncate_label

NORM_SPACES_RE = re.compile(r"\s+")


def normalize_space(str_value):
    """Normalize spaces of the given string.

    This function is named as XSLT function 'normalize-space'.
    See https://developer.mozilla.org/fr/docs/Web/XPath/Fonctions/normalize-space
    Used to ensure that string sent to wikibase are safe.

    >>> norm_space("   \nab\tc  de f  ")
    'ab c de f'
    """
    return NORM_SPACES_RE.sub(" ", str_value).strip()


def create_entity(
    login_instance,
    item_engine,
    label_dict: Dict[str, str],
    description_dict: Dict[str, str],
    aliases_dict: Dict[str, List[str]],
    entity_type: str,
    datatype: str = None,
) -> str:
    """Create a new Wikibase item of property.

    This is a helper function, maybe missing in WikidataIntegrator.
    """

    # Basic checks
    if entity_type not in ("item", "property"):
        raise ValueError(f"Invalid entity type [{entity_type}]")
    if entity_type == "property" and datatype is None:
        raise ValueError(f"Missing datatype info to create property")

    entity = item_engine(new_item=True)

    # Set all labels
    for lang, label in label_dict.items():
        entity.set_label(truncate_label(label), lang=lang)

    # Set all descriptions
    if description_dict:
        for lang, description in description_dict.items():
            entity.set_description(description, lang=lang)

    # Set all aliases
    if aliases_dict:
        for lang, aliases in aliases_dict.items():
            entity.set_aliases(aliases, lang=lang)

    options = {"property_datatype": datatype} if entity_type == "property" else {}

    entity.write(login_instance, entity_type=entity_type, **options)
    return entity.wd_item_id
