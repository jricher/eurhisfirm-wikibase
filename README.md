# EURHISFIRM-Wikibase

## Configuration

```bash
cp .env.template .env
```

### Language setttings

- `DEFAULT_LANGUAGE` defines the language to use for item label when data language is not infered from the database (as "fr" for DFIH database)
- `AVAILABLE_LANGUAGES` defines the language list to use for proper names (as corporation or person name)

## Register script as a MediaWiki bot

- go to the `Special:BotPasswords` special page of your Wikibase instance
- give the name `eurhisfirm-importer` to the bot for example (update `MEDIAWIKI_BOT_USERNAME` accordingly)
- check "High-volume editing", "Edit existing pages" and "Create, edit, and move pages" grants
- copy the generated password to `MEDIAWIKI_BOT_PASSWORD` and set it in `.env`

## Using the CLI

Nobody likes to define entities by hand in Wikibase UI, that's the reason we automated importing properties and items.

### Synchronize base items and properties

Base items and properties used in eurhisfirm-wikibase are defined into `base_entities*.yml` files. These entities have to exist in local wikibase to allow CSV data import. In wikibase, these entities are referenced by their ids (respectively `P*` for properties, `Q*` for items). Those ids can be different from a wikibase instance to another one.

- `base_entities.yml` declares common items and properties
- `base_entities_*.yml` (e.g. `base_entities_scob.yml`, `base_entities_dfih.yml`) declare items and properties proper to a provider

```yaml
properties:
  CORP_ID:
    type: external-id
    labels:
      en: SCOB corporation ID
  CORP_NAME:
    type: string
    labels:
      en: corporation name
```

`base_entities_gen.py` file is automatically generated from `base_entities_*.yml` files and contain local ids for the database. This file is specific to each install and must not be commited into the repo.

Generate (or update) base_entities_gen.py:

```bash
eurhisfirm-wikibase sync-base-entities
```

Note: this command has to be run once prior to first CSV import and every time a `base_entities_*.yml` file is updated.

Python file looks like:

```python
# This file is automatically generated from base_entities_model.yml file
# Please don't edit it manually
# Last generation on 2020-03-02T10:57:54.811577

class BaseItem:
    CORP = 'Q1'
    JOB = 'Q5'
    JURIDISCH_STATUUT = 'Q2'
    LOCATION = 'Q3'
    PERSON = 'Q4'

class BaseProperty:
    CORP_ID = 'P1'
    START_DATE = 'P2'
```

### Importing CSV data

Run

```bash
CSV_FILES_DIR=/path/to/scob/csv eurhisfirm-wikibase import-csv-data import_csv_data scob
CSV_FILES_DIR=/path/to/dfih/csv eurhisfirm-wikibase import-csv-data import_csv_data dfih
```

The missing items will be created, and the existing ones will be updated.
Base items and properties mapping will be retrieve from `base_entities_gen.py`.

#### SCOB import

CSV files:

- scob_corporation_juridisch_statuut.csv
- scob_corporation_locations.csv
- scob_corporation_names.csv
- scob_corporation_sd_ed.csv
- scob_person_function.csv
- scob_persons.csv

#### DFIH import

CSV files:

- dfih_corporation_juridical_status.csv
- dfih_corporation_locations.csv
- dfih_corporation_names.csv
- dfih_corporation_sd_ed.csv
- dfih_person_function.csv
  - column `OLD_PERSON_ID` is ignored
- dfih_person_names_split.csv
  - not used currently, `NAME` information is taken from dfih_persons.csv
- dfih_persons.csv
  - lines with value in `NEW_ID` column are ignored
